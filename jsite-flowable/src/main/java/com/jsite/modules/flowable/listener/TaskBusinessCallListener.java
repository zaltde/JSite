package com.jsite.modules.flowable.listener;

import com.jsite.common.utils.SpringContextHolder;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 监听任务完成后将当前办理人存到全局变量
 * @author JSite
 * @since 2019-11-20
 */
public class TaskBusinessCallListener implements TaskListener {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private RuntimeService runtimeService = SpringContextHolder.getBean(RuntimeService.class);

    @Override
    public void notify(DelegateTask delegateTask) {
//        String proDefKey = delegateTask.getProcessDefinitionId().split(":")[0];
        Map<String, Object> dataMap = runtimeService.getVariables(delegateTask.getProcessInstanceId());
        if (dataMap == null) {
            dataMap = new HashMap<>();
        }

        dataMap.put(delegateTask.getTaskDefinitionKey(), delegateTask.getAssignee());
        runtimeService.setVariables(delegateTask.getProcessInstanceId(), dataMap);


        logger.debug("ProcessInstanceId: [" + delegateTask.getProcessInstanceId() + "] " + "Task Assignee: [" + delegateTask.getAssignee() + "]");
    }
}

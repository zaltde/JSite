/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.service;

import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.flowable.dao.FlowFormBusinessDao;
import com.jsite.modules.flowable.entity.FlowFormBusiness;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 流程表单业务总表Service
 * @author liuruijun
 * @version 2019-04-11
 */
@Service
@Transactional(readOnly = true)
public class FlowFormBusinessService extends CrudService<FlowFormBusinessDao, FlowFormBusiness> {

	@Override
	public FlowFormBusiness get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<FlowFormBusiness> findList(FlowFormBusiness flowFormBusiness) {
		return super.findList(flowFormBusiness);
	}
	
	@Override
	public Page<FlowFormBusiness> findPage(Page<FlowFormBusiness> page, FlowFormBusiness flowFormBusiness) {
		return super.findPage(page, flowFormBusiness);
	}

	@Override
	@Transactional(readOnly = false)
	public void save(FlowFormBusiness flowFormBusiness) {
		super.save(flowFormBusiness);
	}

	@Transactional(readOnly = false)
	public void insert(FlowFormBusiness flowFormBusiness) {
		dao.insert(flowFormBusiness);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(FlowFormBusiness flowFormBusiness) {
		super.delete(flowFormBusiness);
	}
	
}